+++
title = "Konfirmasi Pembayaran"
description = "Konfirmasi pembayaran"
date = "2018-03-30T10:34:07+07:00"
slug = "konfirmasi-pembayaran"
layout = "tentang-kami"
+++

Untuk memudahkan kami dalam mengecek pembayaran Anda, silahkan konfirmasi pembayaran dengan menghubungi Customer Service kami setelah melakukan transfer/setoran ke rekening kami disertai dengan bukti transfer.

Kami akan segera melakukan pengecekan atas transfer Anda dan memproses pengiriman barang Anda.

Terima Kasih.