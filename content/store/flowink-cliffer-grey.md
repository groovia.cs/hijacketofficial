---
title: Flowink Cliffer Grey - CL-GREY
description: Jual jaket Flowink Cliffer Grey - CL-GREY
date: '2018-07-12T17:48:14+07:00'
slug: cl-grey
model:
  - cliffer
brand:
  - flowink
thumbnail: /images/cliffer/cliffer-grey.jpg
image:
  - /images/cliffer/cliffer-grey-1.jpg
  - /images/cliffer/cliffer-grey-2.jpg
  - /images/cliffer/cliffer-grey-3.jpg
  - /images/cliffer/cliffer-grey-4.jpg
  - /images/cliffer/cliffer-grey-5.jpg
  - /images/cliffer/cliffer-grey-6.jpg
sku: CL-GREY
badge: ''
berat: 700 gram
layout: flowink
color:
  - Grey
size:
  - name: All Size
    price: 225000
  - name: XL
    price: 235000
stock: true
---

FLOWINK CLIFFER ORIGINAL Series, adventuring-inspired layer with a design on lightweight fabric for an iconic look. Featuring an oversized pocket at the front, this style pays tribute to our original jackets. It will keep you warm, dry and comfortable during your outdoor adventures.

- • 100 % Polyfiber
- • Imported
- • Machine Wash
- • 75% Waterproof & Windproof
- • Lightweight Fabric for iconic look
- • AE Nets Furing

#### Tabel Ukuran Jaket Flowink Original


| Ukuran          | All Size        | XL              |
|:--------------- |:---------------:|:---------------:|
| Lingkar Dada    | 108-110         | 112-114	      |
| Lingkar Lengan  | 46-48           | 48-50  	      |
| Panjang Tangan  | 60-62           | 62-64  	      |
| Panjang Badan   | 67-69           | 70-72  	      |
