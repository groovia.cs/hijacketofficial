---
title: Hijacket Vendulum Original
description: "Hijacket Vendulum Original merupakan seri hijacket terbaik yang bertema URBAN dilengkapi dengan tali di pinggang, ada 2 saku di dada (sebagai aksesoris) dan 2 saku di perut dan design sablon yang unik dan berkualitas." 
image: "/images/hijacket-vendulum-cover.png"
slug: "vendulum"
---

Hijacket Vendulum Original merupakan seri hijacket terbaik yang bertema URBAN dilengkapi dengan tali di pinggang, ada 2 saku di dada (sebagai aksesoris) dan 2 saku di perut dan design sablon yang unik dan berkualitas. Yuk miliki sekarang warna favorit ukhti!